use reqwest::StatusCode;

use crate::models::Episode;

/// Retrieve all primary information for a given episode. This endpoint allows embedding of additional information. See the section embedding for more information.
pub async fn episode(id: usize) -> Result<Option<Episode>, reqwest::Error> {
    crate::notfoundable_endpoint!(format!("https://api.tvmaze.com/episodes/{}", id,))
}

#[cfg(test)]
mod tests {
    use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

    use super::episode;

    #[tokio::test]
    async fn test_episode() {
        episode(1).await.unwrap().unwrap();
    }

    #[tokio::test]
    async fn test_episode_time_parsing() {
        let episode = episode(1).await.unwrap().unwrap();
        assert_eq!(NaiveDate::from_ymd_opt(2013, 6, 24), episode.airdate);
        assert_eq!(NaiveTime::from_hms_opt(22, 00, 00), episode.airtime);
        assert_eq!(
            NaiveDateTime::new(
                NaiveDate::from_ymd_opt(2013, 6, 25).unwrap(),
                NaiveTime::from_hms_opt(02, 00, 00).unwrap()
            ),
            episode.airstamp.unwrap()
        );
    }
}
