use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
use serde::Deserialize;

use super::{
    time::{parse_date, parse_datetime, parse_time},
    Image, Links, Rating,
};

#[derive(Deserialize, Debug)]
pub struct Episode {
    pub id: usize,
    pub url: String,
    pub name: String,
    pub season: usize,
    pub number: Option<usize>,
    pub r#type: String,
    #[serde(deserialize_with = "parse_date")]
    pub airdate: Option<NaiveDate>,
    #[serde(deserialize_with = "parse_time")]
    pub airtime: Option<NaiveTime>,
    #[serde(deserialize_with = "parse_datetime")]
    pub airstamp: Option<NaiveDateTime>,
    pub runtime: Option<usize>,
    pub rating: Rating,
    pub image: Option<Image>,
    pub summary: Option<String>,
    pub _links: Links,
}
