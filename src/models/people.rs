use chrono::NaiveDate;
use serde::Deserialize;

use super::{time::parse_date, Country, Image, Links};

#[derive(Deserialize, Debug)]
pub struct Person {
    pub id: usize,
    pub url: String,
    pub name: String,
    pub country: Option<Country>,
    #[serde(deserialize_with = "parse_date")]
    pub birthday: Option<NaiveDate>,
    #[serde(deserialize_with = "parse_date")]
    pub deathday: Option<NaiveDate>,
    pub gender: Option<String>,
    pub image: Option<Image>,
    pub updated: usize,
    pub _links: Links,
}

#[derive(Deserialize, Debug)]
pub struct CrewCredit {
    pub r#type: String,
    pub _links: Links,
}

#[derive(Deserialize, Debug)]
pub struct CastCredit {
    #[serde(rename = "self")]
    pub _self: bool,
    pub voice: bool,
    pub _links: Links,
}
