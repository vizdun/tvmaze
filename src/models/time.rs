use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
use serde::{Deserialize, Deserializer};

#[derive(Deserialize, Debug)]
struct Empty {}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum Aux {
    T(String),
    Empty(Empty),
    Null,
}

pub fn parse_time<'de, D>(d: D) -> Result<Option<chrono::NaiveTime>, D::Error>
where
    D: Deserializer<'de>,
{
    match Deserialize::deserialize(d)? {
        Aux::T(s) => Ok(NaiveTime::parse_from_str(&s, "%H:%M").ok()),
        Aux::Empty(_) | Aux::Null => Ok(None),
    }
}

pub fn parse_date<'de, D>(d: D) -> Result<Option<chrono::NaiveDate>, D::Error>
where
    D: Deserializer<'de>,
{
    match Deserialize::deserialize(d)? {
        Aux::T(s) => Ok(NaiveDate::parse_from_str(&s, "%Y-%m-%d").ok()),
        Aux::Empty(_) | Aux::Null => Ok(None),
    }
}

pub fn parse_datetime<'de, D>(d: D) -> Result<Option<chrono::NaiveDateTime>, D::Error>
where
    D: Deserializer<'de>,
{
    match Deserialize::deserialize(d)? {
        Aux::T(s) => Ok(NaiveDateTime::parse_from_str(&s, "%Y-%m-%dT%H:%M:%S%:z").ok()),
        Aux::Empty(_) | Aux::Null => Ok(None),
    }
}

mod tests {}
